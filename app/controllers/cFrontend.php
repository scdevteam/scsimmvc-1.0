<?php

/**
 *  cFrontend
 */
class Frontend extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function home()
	{
		$this->view->render('2', 'home');
	}

	public function about()
	{
		$this->view->render('2', 'about');
	}

	public function contact()
	{
		$this->view->render('2', 'contact');
	}

	public function project()
	{
			$this->view->render('2', 'pjroject');
	}

	public function blogPost($id = array(), $title = array() )
	{

		$data = array( 'id' => $id , 'title' => $title);
		$this->view->render('1', 'blogPost' , $data);
	}
}
