<?php
// ------------------------------ THE WRAPPER -------------------------------------------

/**
 *       remember $this->  refers to its self the class App
 */
class App extends Functions
{
    //Default Controller and Method
	//params could be the language choice..en,nl,fr,du....
	private static $controller = 'Frontend';
	private static $method = 'home';
	private static $params = [];
    

    public function __construct()
    {
        parent::__construct();
		self::init();
		self::autoload(); 
        self::router();
		self::dispatcher();
    }

    private static function init()
    {
        //INITIALIZING      
        define("PATH_ROOT" , getcwd() . DS);                    
        define("PATH_HTML" , PATH_ROOT . "html" . DS);

        define("PATH_CONFIG" , PATH_APP . "config" . DS);
        define("PATH_LIBS" , PATH_APP . "libs" . DS);
        define("PATH_CONTROLLER" , PATH_APP . "controllers" . DS);
        define("PATH_MODEL", PATH_APP . "models" . DS);
        define("PATH_VIEW", PATH_APP . "views" . DS);

        define("PATH_F_VIEW", PATH_VIEW . "frontend" . DS);
        define("PATH_F_VIEW_TMP", PATH_F_VIEW. "templates" . DS);
        define("PATH_B_VIEW", PATH_VIEW . "backend" . DS);
        define("PATH_B_VIEW_TMP", PATH_B_VIEW. "templates" . DS);

        define("PATH_UPLOAD", PATH_HTML . "uploads" . DS);
        define("PATH_TMP", PATH_HTML . "tmp" . DS);


        //2.    define default platform,controller,method 
        define("DEF_F_CONTROLLER" , "Frontend");
        define("DEF_B_CONTROLLER" , "Backend");
        define("DEF_F_METHOD", "home");      
        define("DEF_B_METHOD", "dashboard");         
        /*
            developers choice not to use the method index
            simply to many index pages to keep track of
        */

        //3. UNCOMMENT FOR TESTING PATHS  
        // echo '<br> PATH_ROOT ::'.PATH_ROOT;
        // echo '<br> PATH_APP :: '.PATH_APP;
        // echo '<br> PATH_CORE :: '.PATH_CORE;
        // echo '<br> PATH_CONFIG ::'.PATH_CONFIG;
        // echo "<pre>";
        // print_r($_SERVER);
        // echo "</pre>";   

        //3.    Load configuration file
        $GLOBALS['config'] = include PATH_CONFIG . "config.php";

        //4.    Load core classes
        // require PATH_LIBS . "functions.php";
        // self::$func = new Functions();

        require PATH_CORE . "Controller.php";
        require PATH_CORE . "Model.php";
        require PATH_CORE . "View.php";

       

        //5.    session start
        session_start();

    }

    private static function autoload()
    {
        //AUTOLOADER WITH CUSTOM LOAD METHOD
        spl_autoload_register(function ($class) 
        {
            //echo "$class";
            if ( $class === "Frontend" || $class === "Backend")
            {                
                require_once PATH_CONTROLLER . "c" . $class . ".php";
            }
            else
            {
                require_once PATH_MODEL . "m" . $class . ".php";
            }
        });

    }


    private static function router()
    {
        //ROUTING
        $tokens = self::parseUrl();  //functions from the parent class Functions

        // echo "<pre>";
        // print_r($tokens);
        // echo "</pre>";  

        //THE CONTROLLER    $tokens[0]  f or b
        //THE METHOD        $tokens[1]  home, about, etc...
        //THE PARAMS        $tokens[n]  eg. blog post page id number 

        // if no controller and method is given, the default router sets them to the default f/home ... cool
        // so if isset .... its always set by the default router.
        // nor can it be empty

        // PATH_CONTROLLER . "cFrontend.php"  
        // PATH_CONTROLLER . "cBackend.php"  
        // PATH_CONTROLLER . "cError.php"  

        // controller is F => Frontend();
        // controller is B => Backend();

        //THE CONTROLLER
        $t = strtolower( $tokens[0] );   
        $c = strlen ( $t ); 
        if ($c == 1 && $t === 'f')
        {

            $file = PATH_CONTROLLER . "cFrontend.php";
            if ( file_exists ( $file ))
            {
                self::$controller = DEF_F_CONTROLLER;
                self::$controller = new self::$controller;
                if(method_exists(self::$controller, $tokens[1]))
                {
                    self::$method = $tokens[1];
                }
                else
                {
                    self::$method = DEF_F_METHOD;
                }
                // unset removes it from the array
                unset( $tokens[0] );  
                unset( $tokens[1] );
            }

        }
        elseif ($c == 1 && $t === 'b')
        {

            $file = PATH_CONTROLLER . "cBackend.php";
            if ( file_exists ( $file ))
            {
                self::$controller = DEF_B_CONTROLLER;
                self::$controller = new self::$controller;
                if(method_exists(self::$controller, $tokens[1]))
                {
                    self::$method = $tokens[1];
                }
                else
                {
                    self::$method = DEF_B_METHOD;
                }
                // unset removes it from the array
                unset( $tokens[0] );  
                unset( $tokens[1] );
            }

        }
        else
        {
            $file = PATH_CONTROLLER . "cFrontend.php";
            if ( file_exists ( $file ))
            {
                self::$controller = DEF_F_CONTROLLER ;
                self::$controller = new self::$controller();
                self::$method = DEF_F_METHOD;
            }
                // unset removes it from the array
                unset( $tokens );
        }


         //rebase the left over values in tkoen "array_values" into params 
         //the if tokens has values the rebase otherwise an empty arry
         //due to the effect that we unset the token [0][1] and if token [3][4] exist
         // rebase them to params [0][1]
		 self::$params = $tokens ? array_values($tokens) : [];

    }
    
    private static function dispatcher()
    {
        //DISPATCHING
         call_user_func_array([self::$controller, self::$method], self::$params );

         //calls the controller and the method within that controller and passes the params into that method 
         // eg.    home -> index (params[0],params[2],params[3],....... etc)
    }


} //END CLASS

?>